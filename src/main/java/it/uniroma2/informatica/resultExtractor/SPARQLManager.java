package it.uniroma2.informatica.resultExtractor;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.QueryEvaluationException;
import it.uniroma2.art.owlart.exceptions.UnsupportedQueryLanguageException;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.models.OWLArtModelFactory;
import it.uniroma2.art.owlart.models.TripleQueryModelHTTPConnection;
import it.uniroma2.art.owlart.query.MalformedQueryException;
import it.uniroma2.art.owlart.query.QueryLanguage;
import it.uniroma2.art.owlart.query.TupleBindings;
import it.uniroma2.art.owlart.query.TupleBindingsIterator;
import it.uniroma2.art.owlart.query.TupleQuery;
import it.uniroma2.informatica.domain.Query;
import it.uniroma2.informatica.domain.StringVariable;
import it.uniroma2.informatica.domain.TripleElement;
import it.uniroma2.informatica.domain.Variable;
import it.uniroma2.informatica.uimanager.UIManager;
import it.uniroma2.informatica.utils.Preconditions;
import it.uniroma2.informatica.utils.SystemConstants;

import java.util.*;

import org.apache.log4j.Logger;


/**
 * La classe gestisce l'esecuzione delle query sparql su dbpedia o su ontologia esterna. Sono presenti metodi per la gestione della
 * conversione delle query in query SPARQL
 * Created by Giovanni Lorenzo Napoleoni on 14/12/2014.
 */
public class SPARQLManager {

    static Logger log = Logger.getLogger(SPARQLManager.class.getName());

    /**
     * Conversione di una lista di query in una query sparql
     * @param queries query da convertire
     * @return query in SPARQL
     */
    public String convertQueriesToSparqlQuery(List<Query> queries){

        //Preconditions
        Preconditions.checkNotNull(queries, "Lista di query nulla");

        //Costrusici un nuovo string builder
        StringBuilder transformedQuery = new StringBuilder();

        //Costruzione intestazione query
        transformedQuery.append(SystemConstants.SPARQL_SELECT_LABEL);
        transformedQuery.append(" * ");
        transformedQuery.append(SystemConstants.SPARQL_WHERE_LABEL);
        transformedQuery.append("{ \n");
        //Per ogni query nella lista di query viene  presa la tripla e trasformata in una condizione della clausola WHERE
        for (Query query: queries){

            TripleElement subject = query.getmTriple().getmSubject();
            TripleElement predicate = query.getmTriple().getmPredicate();
            TripleElement object = query.getmTriple().getmObject();

            //Preconditions
            Preconditions.checkNotNull(subject,"Il soggetto della tripla e' null");
            Preconditions.checkNotNull(predicate,"Il predicato della tripla e' null");
            Preconditions.checkNotNull(object,"L'oggetto della tripla e' null");

            //Coverti l'elemento nella tripla nella corrispondente rappresentazione in SPARQL
            transformedQuery.append(convertTripleElementToSPARQLElement(object,true));
            transformedQuery.append(convertTripleElementToSPARQLElement(predicate,false));
            transformedQuery.append(convertTripleElementToSPARQLElement(subject,true));

            transformedQuery.append(". \n");
        }
        transformedQuery.append("}");

        log.info(transformedQuery.toString());
        log.debug(transformedQuery.toString());
        UIManager.getIstance().getGui().addLog(transformedQuery.toString());

        // return sparql query
        return transformedQuery.toString();

    }

    /**
     * Estrai della lista di query la variabile da cui estrarre i risultati dopo l'esecuzione della query. La variabile
     * a cui siamo interessati è la prima che compare nella lista. Se la variabile non esiste, significa che la query e' di tipo
     * ASK
     * @param queries lista di query
     * @return la variabile se esiste.
     */
    public String exractFirstVariabile(List<Query> queries){
        //Preconditions
        Preconditions.checkNotNull(queries, "Lista di query nulla");

        String var= "";
        //Per ogni query controlla se esistono variabile. Appena viene trovata la prima variabile il ciclo viene interrotto
        for (Query query: queries){
            TripleElement subject = query.getmTriple().getmSubject();
            TripleElement predicate = query.getmTriple().getmPredicate();
            TripleElement object = query.getmTriple().getmObject();
            //Preconditions
            Preconditions.checkNotNull(subject,"Il soggetto della tripla e' null");
            Preconditions.checkNotNull(predicate,"Il predicato della tripla e' null");
            Preconditions.checkNotNull(object,"L'oggetto della tripla e' null");

            if ( subject instanceof Variable){
                var = subject.getRepresentation();
                break;
            }
            if (predicate instanceof Variable){
                var = predicate.getRepresentation();
                break;
            }
            if(object instanceof Variable){
                var = object.getRepresentation();
                break;
            }
        }
        return var;
    }

    /**
     * Controlla se la query è di tipo ASK
     * @param query query da controllare
     * @return true se e' una query ASK, false altrimenti
     */
    public boolean isAnAskQuery(List<Query> query){
        String string = exractFirstVariabile(query);
        if (string == ""){
            return true;
        }
        return false;
    }

    /**
     * Esegue una query sparql su DBPEDIA.
     * @param querySPARQL query da eseguire
     * @param endpoint endpoint di dbedia
     * @param owlArtModelFactory factory per la conessione SPARQL
     * @return una mappa di risultati con chiave uguale alla variabile
     * @throws ModelCreationException
     * @throws UnsupportedQueryLanguageException
     * @throws MalformedQueryException
     * @throws ModelAccessException
     * @throws QueryEvaluationException
     */
    public Map<String,List<ARTNode>> executeSPARQLQuery(String querySPARQL, String endpoint, OWLArtModelFactory owlArtModelFactory,boolean isAsk) throws ModelCreationException, UnsupportedQueryLanguageException, MalformedQueryException, ModelAccessException, QueryEvaluationException {

        //Preconditions
        Preconditions.checkNotNull(owlArtModelFactory,"La OWLFActory e' null");
        //Contenitore per i risultati della query. La coppie <K,V> rappresentano le variabili della query e i valori ad esse associati.
        Map<String,List<ARTNode>> resultsOfSPARQLQuery = new HashMap<>();

        TripleQueryModelHTTPConnection connection;
        connection = owlArtModelFactory.loadTripleQueryHTTPConnection(endpoint);
        TupleQuery query = connection.createTupleQuery(QueryLanguage.SPARQL, querySPARQL, "");
        TupleBindingsIterator itRes = query.evaluate(false);
        //Per ogni tupla estrai il risultato delle variabili
        while (itRes.hasNext()) {
            TupleBindings tuple = itRes.next();
            //Per ogni variabile della tupla ottieni il risultato associato
            for (String bindName : tuple.getBindingNames()) {
                //Salva il valore nella mappa
                ARTNode artNode = tuple.getBoundValue(bindName);
                addResultToSPARQLMap(resultsOfSPARQLQuery, bindName, artNode);
            }
        }
        connection.disconnect();

        return resultsOfSPARQLQuery;
    }

    /**
     * Aggiunge uno specifico risultato alla mappa dei risultati associati ad una query sparql
     * @param resultsOfSPARQLQuery mappa dei risultati
     * @param value valore da aggiungere
     * @param node variabile a cui deve essere associato il risultato
     */
    private void addResultToSPARQLMap(Map<String, List<ARTNode>> resultsOfSPARQLQuery, String value, ARTNode node){
        if (resultsOfSPARQLQuery.get(value) != null){
            resultsOfSPARQLQuery.get(value).add(node);
        }
        else{
            resultsOfSPARQLQuery.put(value, new ArrayList<ARTNode>());
            resultsOfSPARQLQuery.get(value).add(node);
        }
    }

    /**
     * Converti un elemento di tipo TripleElement nella sua corrispondente rappresentazione in SPARQL, controllando se è di tipo resource
     * @param element elemento da convertire
     * @param isResource flag che esprime se element è una risorsa
     * @return l'elemento convertito
     */
    private String convertTripleElementToSPARQLElement(TripleElement element,boolean isResource){
        //Controlla se uno dei tre campi della tripla è una variabile
        if(element instanceof Variable || element instanceof StringVariable){
           return " ?"+resourceAdjoust(element.getRepresentation());
        }else {
            if(isResource){
                return " <"+SystemConstants.DBPEDIA_URI_RESOURCE+resourceAdjoust(element.getRepresentation())+"> ";
            }else{
                return " <"+SystemConstants.DBPEDIA_URI_PROPERTY+resourceAdjoust(element.getRepresentation())+"> ";
            }

        }

    }


    /*
     * @param List<String> lista di stringhe da unire e raffinare
     * @return String stringa con ' ' sostituiti da '_' e senza eventuali sottostringhe 'the'
     */
    private String resourceAdjust(List<String> subquestion)
    {
        String result = new String();

        for(int i= 0;i<subquestion.size();i++)
        {
            if(subquestion.get(i).equals("the"))
                continue;
            result += subquestion.get(i)+"_";
        }

        return result.substring(0, result.length()-1);
    }

    private String resourceAdjoust(String subquestion)
    {
        return resourceAdjust(Arrays.asList(subquestion.split(" |\\?|\\'")));
    }
}
