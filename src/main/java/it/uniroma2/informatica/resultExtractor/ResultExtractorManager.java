package it.uniroma2.informatica.resultExtractor;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.QueryEvaluationException;
import it.uniroma2.art.owlart.exceptions.UnsupportedQueryLanguageException;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.impl.ARTURIResourceEmptyImpl;
import it.uniroma2.art.owlart.models.OWLArtModelFactory;
import it.uniroma2.art.owlart.query.MalformedQueryException;
import it.uniroma2.art.owlart.sesame2impl.factory.ARTModelFactorySesame2Impl;
import it.uniroma2.art.owlart.sesame2impl.models.conf.Sesame2ModelConfiguration;
import it.uniroma2.informatica.domain.*;
import it.uniroma2.informatica.utils.Preconditions;
import it.uniroma2.informatica.utils.SystemConstants;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Gestore per l'esecuzione della query inserita dall'utente.
 * Created by Giovanni Lorenzo Napoleoni on 13/12/2014.
 */
public class ResultExtractorManager {

    //Gestore per la gestione e l'esecuzione delle query SPARQL
    private SPARQLManager mSparqlManager;

    //Factory per l'esecuzione delle query
    private OWLArtModelFactory<Sesame2ModelConfiguration> factory;

    public ResultExtractorManager() {

        mSparqlManager = new SPARQLManager();
        factory = OWLArtModelFactory.createModelFactory(new ARTModelFactorySesame2Impl());
    }

    /**
     * Esecuzione della domanda convertita in una lista di query.
     * @param queries query da eseguire
     * @return risultati della query.
     */
    public List<QueryResult> executeQuery(List<Query> queries) throws UnsupportedQueryLanguageException, QueryEvaluationException, MalformedQueryException, ModelCreationException, ModelAccessException {

        //Preconditions
        Preconditions.checkNotNull(queries,"Lista di query nulla");

        //Costruisci una lista di QueryResult per memorizzare i risultati della ricerca
        List<QueryResult> queryResults = new ArrayList<QueryResult>();
        //Estrai la variabile di cui si vuole ottenere il valore
        String var = mSparqlManager.exractFirstVariabile(queries);
        //Costruisci la query SPARQL
        String sparqlQuery = mSparqlManager.convertQueriesToSparqlQuery(queries);
        //Esegui la query SPARQL
        Map<String,List<ARTNode>> results = mSparqlManager.executeSPARQLQuery(sparqlQuery,SystemConstants.DBPEDIA_ENDPOINT,factory,mSparqlManager.isAnAskQuery(queries));

        //Se la variabile principale è vuota, allora la domanda è di tipo ASK Query. Setta il valore di var a "star_fake"
        if (var.equals("")){
            var = SystemConstants.SPARQL_ASK_QUERY_IDENTIFIER;
        }
        //Estrai i valori della variabile principale dalla mappa dei risultati
        if(results.get(var) != null){
            for (ARTNode node : results.get(var)){
                if(var.equals(SystemConstants.SPARQL_ASK_QUERY_IDENTIFIER)){
                    ARTNode nodeASK = new ARTURIResourceEmptyImpl("true");
                    QueryResult queryResult = new QueryResult(nodeASK);
                    queryResults.add(queryResult);
                }
                else{
                    QueryResult queryResult = new QueryResult(node);
                    queryResults.add(queryResult);
                }

            }
        }
        return queryResults;
    }

    /**
     * Metodo per ottenere tutte le proprietà di una risorsa specificata in input.
     * @param resource resource di cui vogliamo ottenere le properties
     * @return lista di Triple contenente tutte le properties della risorsa
     * @throws UnsupportedQueryLanguageException
     * @throws QueryEvaluationException
     * @throws MalformedQueryException
     * @throws ModelCreationException
     * @throws ModelAccessException
     */
    public List<ConcreteTriple> getPropertiesOfResource(String resource) throws UnsupportedQueryLanguageException, QueryEvaluationException, MalformedQueryException, ModelCreationException, ModelAccessException {
        //Inserimento della risorsa in lista di query
        List<Query> queries = new ArrayList<>();
        //TODO DA RIVEDERE LA GESTIONE DI STRING_VARIABLE
        TripleElement subject = new StringVariable("x");
        TripleElement predicate = new StringVariable("y");
        TripleElement object = new Resource(resource);

        Triple triple = new Triple(subject,predicate,object);
        Query query = new Query(triple);
        queries.add(query);

        //Conversione della lista di query in una query sparql
        String sparqlQuery = mSparqlManager.convertQueriesToSparqlQuery(queries);
        //Esegui la query SPARQL
        Map<String,List<ARTNode>> results = mSparqlManager.executeSPARQLQuery(sparqlQuery,SystemConstants.DBPEDIA_ENDPOINT,factory,false);
        //Restituzione delle proprietà
        return extractResultFromPropertySPARQL(results,triple);
    }

    /**
     * Estrae le proprietà dalla query sparql associata alle proprietà di una determinata risorsa
     * @param results risultati della query sparql
     * @param triple tripla che contiene la risorsa di cui vogliamo ottenere la proprietà.
     * @return
     */
    private List<ConcreteTriple> extractResultFromPropertySPARQL(Map<String,List<ARTNode>> results, Triple triple){
        //Controlla se ci sono risultati utili, altrimenti torna una lista vuota
        if (results.size() == 0){
            return new ArrayList<>();
        }
        List<ConcreteTriple> concreteTriples = new ArrayList<>();
        ARTNode artNodeSubject = new ARTURIResourceEmptyImpl(SystemConstants.DBPEDIA_URI_RESOURCE+triple.getmObject().getRepresentation());
        for (ARTNode node: results.get(triple.getmSubject().getRepresentation())){
            ConcreteTriple concreteTriple = new ConcreteTriple(artNodeSubject,null,node);
            concreteTriples.add(concreteTriple);
        }
        List<ARTNode> artNodes = results.get(triple.getmPredicate().getRepresentation());
        for (int i = 0; i < artNodes.size();i++){
          concreteTriples.get(i).setmPredicate(artNodes.get(i));
        }

        return  concreteTriples;
    }

    public SPARQLManager getSparqlManager() {
        return mSparqlManager;
    }

    public void setSparqlManager(SPARQLManager mSparqlManager) {
        this.mSparqlManager = mSparqlManager;
    }
}