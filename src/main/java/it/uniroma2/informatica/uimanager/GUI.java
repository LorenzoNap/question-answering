package it.uniroma2.informatica.uimanager;

import it.uniroma2.informatica.domain.QueryResult;
import it.uniroma2.informatica.systemController.SystemController;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * Graphic user interface for Question answering system.
 * Created by Giovanni Lorenzo Napoleonion 13/01/15.
 */
public class GUI {


    private SystemController systemController;
    private UIManager uiManager;
    private JFrame mainFrame;
    private JTextArea displayLogArea;

    public GUI(SystemController systemController) {
        this.systemController = systemController;
        this.uiManager = UIManager.getIstance();
        makeFrame();
    }

    public GUI() {
        makeFrame();
    }

    /**
     *
     */
    private void makeFrame() {
        //Create main Frame
        mainFrame = new JFrame("Question answering");
        //create menu bar
        createMenuBar();
        //get content pane
        Container contentPane = mainFrame.getContentPane();
        //Set layout of content pane
        contentPane.setLayout(new BorderLayout());

        //Create Response panel area
        JPanel responseArea = new JPanel ();
        responseArea.setLayout(new BorderLayout());
        responseArea.setBorder(new TitledBorder(new EtchedBorder(), "Response Area"));
        // create the middle panel components
        final JTextArea displayResponse = new JTextArea ( 16, 50 );

        displayResponse.setEditable(false); // set textArea non-editable
        JScrollPane scrollResponse = new JScrollPane ( displayResponse );
        scrollResponse.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        //Add Textarea in to middle panel
        responseArea.add(scrollResponse);
        contentPane.add(responseArea,BorderLayout.CENTER);

        //create insert panel question
        JPanel northPanel = new JPanel();
        northPanel.setBorder(new TitledBorder(new EtchedBorder(), "Insert Question"));
        northPanel.setLayout(new GridLayout(4,1));
        contentPane.add(northPanel,BorderLayout.NORTH);
        final JTextField questionArea = new JTextField();
        northPanel.add(questionArea);

        JLabel suggestedQuestionsLabel = new JLabel("Suggested Questions");
        northPanel.add(suggestedQuestionsLabel);
        String[] suggestedQuestionsStrings = { "What is the capital of Italy?","Is Rome the capital of Italy?","What is the population of Rome?","Who is the mayor of Rome?",
                "Who is the architect of the Eiffel Tower?","Who is the artist of the Mona Lisa?","Who is the designer of the Eiffel Tower?",
                "Who is the creator of the Mona Lisa?","Who is the artist of the amazing Mona Lisa?","Who is the famous creator of the Mona Lisa?",
                "Who is the mayor of the capital of Italy?"};

        //Create the combo box, select item at index 4.
        //Indices start at 0, so 4 specifies the pig.
        JComboBox<String> suggestedQuestions = new JComboBox<String>(suggestedQuestionsStrings);
        suggestedQuestions.setSelectedIndex(4);
        suggestedQuestions.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	@SuppressWarnings("unchecked")
				JComboBox<String> cb = (JComboBox<String>) e.getSource();
                String suggestionQuestion = (String) cb.getSelectedItem();
                questionArea.setText(suggestionQuestion);
            }
        });
        northPanel.add(suggestedQuestions);

        JButton button = new JButton("Submit");
        //add button to submit query
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                displayResponse.setText("");
                //check if input is empty
                if (questionArea.getText().equals("") || questionArea.getText().equals(" ")) {
                    createAlertDialog("Warning: your input is empty!");
                } else {
                    //Start system controller
                    addLog("Question: " + questionArea.getText());
                    java.util.List<QueryResult> queryResultList = systemController.executeApplication(questionArea.getText());
                    if (queryResultList.isEmpty()) {
                        displayResponse.setText("No results were found.");
                        System.out.println("No results were found.");
                    } else {
                    	
                    	List<String> noDuplicateResults = uiManager.getResultsWithoutDuplicatesForGUI(queryResultList);
                        String string = "Results:\n";
                        for (String result : noDuplicateResults) {
                            string += result + "\n";
                        }
                        displayResponse.setText(string.replace("_"," "));
                    }
                }

            }
        });
        northPanel.add(button);

        //ParamsButton
        JPanel paramsPanel = new JPanel ();
        paramsPanel.setBorder(new TitledBorder(new EtchedBorder(), "Params"));
        paramsPanel.setLayout(new GridLayout(4,0));
        JCheckBox setTemplate = new JCheckBox("TemplateParse");
        setTemplate.addItemListener(new ItemListener() {

            public void itemStateChanged(ItemEvent e) {
                int value = e.getStateChange();
                if (value == 1){
                    systemController.getInstanceOfConfigurationHandler().setTemplateParser("true");
                }else{
                    systemController.getInstanceOfConfigurationHandler().setTemplateParser("false");
                }
            }
        });
        paramsPanel.add(setTemplate);
        JCheckBox setSynonimer = new JCheckBox("Synonimer");
        setSynonimer.addItemListener(new ItemListener() {

            public void itemStateChanged(ItemEvent e) {
                int value = e.getStateChange();
                if (value == 1) {
                    systemController.getInstanceOfConfigurationHandler().setSynonimer("true");
                } else {
                    systemController.getInstanceOfConfigurationHandler().setSynonimer("false");
                }
            }
        });
        paramsPanel.add(setSynonimer);
        JCheckBox setSyntacticParser = new JCheckBox("SyntacticParser");
        setSyntacticParser.addItemListener(new ItemListener() {

            public void itemStateChanged(ItemEvent e) {
                int value = e.getStateChange();
                if (value == 1) {
                    systemController.getInstanceOfConfigurationHandler().setSyntacticParser("true");
                } else {
                    systemController.getInstanceOfConfigurationHandler().setSyntacticParser("false");
                }
            }
        });
        paramsPanel.add(setSyntacticParser);
        contentPane.add(paramsPanel,BorderLayout.EAST);

        initializeParams(setTemplate, setSynonimer, setSyntacticParser);

        //Create log panel area
        JPanel logArea = new JPanel ();
        logArea.setLayout(new BorderLayout());
        logArea.setBorder(new TitledBorder(new EtchedBorder(), "Log Area"));
        // create the middle panel components
        displayLogArea = new JTextArea (16,50);
        displayLogArea.setEditable(false); // set textArea non-editable
        JScrollPane scrollLog = new JScrollPane ( displayLogArea );
        scrollLog.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        //Add Textarea in to middle panel
        JButton buttonClearLog = new JButton("Clear log");
        buttonClearLog.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                displayLogArea.setText("");
            }
        });
        logArea.add(buttonClearLog,BorderLayout.SOUTH);
        logArea.add(scrollLog);
        contentPane.add(logArea,BorderLayout.SOUTH);


        addLog("Welcome To Question Answering System");

        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        mainFrame.setPreferredSize(new Dimension(1200, 800));
        mainFrame.pack();
        mainFrame.setVisible(true);
    }

    private void initializeParams(JCheckBox setTemplate, JCheckBox setSynonymer, JCheckBox setSyntacticParser) {

        setTemplate.setSelected(Boolean.valueOf(systemController.getInstanceOfConfigurationHandler().getTemplateParser()));
        setSynonymer.setSelected(Boolean.valueOf(systemController.getInstanceOfConfigurationHandler().getSynonimer()));
        setSyntacticParser.setSelected(Boolean.valueOf(systemController.getInstanceOfConfigurationHandler().getSyntacticParser()));
    }

    public void createAlertDialog(String message){
        JOptionPane.showMessageDialog(mainFrame, message, "WARNING", JOptionPane.WARNING_MESSAGE);
    }

    public void addLog(String log){
        displayLogArea.append(log+"\n");
    }


    private void createMenuBar(){
        JMenuBar menuBar = new JMenuBar();
        mainFrame.setJMenuBar(menuBar);

        JMenu fileMenu = new JMenu("File");
        menuBar.add(fileMenu);
    }
}
