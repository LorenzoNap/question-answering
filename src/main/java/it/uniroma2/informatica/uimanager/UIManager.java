package it.uniroma2.informatica.uimanager;

import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.informatica.domain.BinaryTree;
import it.uniroma2.informatica.domain.Node;
import it.uniroma2.informatica.domain.QueryResult;
import it.uniroma2.informatica.domain.UserInput;
import it.uniroma2.informatica.systemController.SystemController;

import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.abego.treelayout.TreeForTreeLayout;
import org.abego.treelayout.TreeLayout;
import org.abego.treelayout.util.DefaultConfiguration;
import org.abego.treelayout.util.DefaultTreeForTreeLayout;
import org.apache.log4j.Logger;

import javax.swing.*;

/**
 * Gestore dell'input e dell'output. La classe si occupa di gestire le informazioni inserite dall'untente e per la restituzione dei risultati della "question".
 * Created by Giovanni Lorenzo Napoleoni on 13/12/2014.
 */
public class UIManager {

	static Logger log = Logger.getLogger(UIManager.class.getName());

	private static UIManager uiManagerInstance;

	private BufferedReader myInput;

	private GUI gui;

	public static UIManager getIstance(){
		if(uiManagerInstance == null){
			uiManagerInstance = new UIManager();
		}
		return uiManagerInstance;
	}

	private UIManager(){
		myInput = new BufferedReader(new InputStreamReader(System.in));
	}

	private String readLineFromUser() throws IOException{
		String inputQuery;

		inputQuery = myInput.readLine();

		return inputQuery;
	}

	public void createGUI(SystemController systemController){

		gui = new GUI(systemController);
	}


	/**
	 * Take an input from the user and returns a UserInput
	 * @return Input from the user in object UserInput
	 * @throws IOException
	 */
	public UserInput readInputFromUser() throws IOException {

		UserInput input = null;

		System.out.println("Insert a query:");

		String inputQuery = readLineFromUser();

		input = new UserInput(inputQuery);
		return input;
	}


	/**
	 * Visualize result to user
	 * @param
	 */
	public void visualizeResults(List<QueryResult> listQueryResults) {
		if(listQueryResults.isEmpty()){
			System.out.println("No results were found.");
		} else {

			// Get results as String distinguishing from URI and Literals.
			// Remove duplicates also.

			List<String> listResults = getResultsWithoutDuplicates(listQueryResults);

			System.err.println("Results:");
			for(String result : listResults){
				System.err.println(result);
			}

		}
	}

	private List<String> getResultsWithoutDuplicates(List<QueryResult> listResults){
		List<String> noDuplicateResults = new ArrayList<String>();

		for(QueryResult queryResult : listResults){
			ARTNode artNode = queryResult.getValue();
			if(artNode.isURIResource()){
				String localName = artNode.asURIResource().getLocalName();
				if(!noDuplicateResults.contains(localName)){
					noDuplicateResults.add(localName);
				}
			} else if(artNode.isLiteral()){
				String label = artNode.asLiteral().getLabel();
				String lang = artNode.asLiteral().getLanguage();
				String literalResult = label + " " + lang;
				if(!noDuplicateResults.contains(literalResult)){
					noDuplicateResults.add(literalResult);
				}
			}
		}

		return noDuplicateResults;
	}
	
	public List<String> getResultsWithoutDuplicatesForGUI(List<QueryResult> listResults){
		return uiManagerInstance.getResultsWithoutDuplicates(listResults);
	}

	/**
	 * Ask for termination.
	 * @return true if the user say yes.
	 * @throws IOException
	 */
	public boolean askforTermination() throws IOException {
		System.out.println("Do you want to submit another query?");

		String termination = readLineFromUser();

		System.out.println("User insert: " + termination);

		if(termination.toLowerCase().equals("yes")){
			return false;
		}
		else {
			return true;
		}
	}

	/**
	 * Convert from URI to String. Eliminate also the _ substituing with space.
	 * @param uri
	 * @return String
	 */
	public static String convertFromURI(String uri){
		//Splitto la stringa in base al simbolo /
		List<String> splittedUri = new ArrayList<String>(Arrays.asList(uri.split("\\/")));
		//Trasformo l'ultima stringa (contenente il risultato) in un array di char
		char[] splittedString = splittedUri.get(splittedUri.size()-1).toCharArray();
		//Sostituisco gli _ con gli spazi
		for(int i=0; i<splittedString.length; i++)
			if (splittedString[i]=='_')
				splittedString[i]=' ';
		return new String(splittedString);
	}

	public static String getResourceFromArtNode(ARTNode artNode){

		if(artNode.isURIResource()){
			String localName = artNode.asURIResource().getLocalName();
			return localName;
		} else if(artNode.isLiteral()){
			String label = artNode.asLiteral().getLabel();
			String lang = artNode.asLiteral().getLanguage();
			return label + " " + lang;
		}
		return "";
	}

	public void showErrorOnGUI(String message){
		System.out.println(message);
		getGui().createAlertDialog(message);
	}


    public void ric(DefaultTreeForTreeLayout<TextInBox> tree,TextInBox father,Node child){
        TextInBox nodeTree;
        if(child.getChildren().size() <= 0){
            nodeTree = new TextInBox(child.getData(), 60, 20);
        }
        nodeTree = new TextInBox(child.getData(), 60, 20);
        tree.addChild(father,nodeTree);
        for( Node node: child.getChildren()){
            ric(tree,nodeTree,node);
        }


    }
    public  void printGUITree(BinaryTree mBinaryTree){

        if (mBinaryTree != null || mBinaryTree.getRoot() != null){
        TextInBox root = new TextInBox(mBinaryTree.getRoot().getData(), 60, 20);
        DefaultTreeForTreeLayout<TextInBox> tree = new DefaultTreeForTreeLayout<TextInBox>(root);
        for(Node node: mBinaryTree.getRoot().getChildren()){
            ric(tree,root,node);
        }
        // setup the tree layout configuration
        double gapBetweenLevels = 30;
        double gapBetweenNodes = 10;
        DefaultConfiguration<TextInBox> configuration = new DefaultConfiguration<TextInBox>(
                gapBetweenLevels, gapBetweenNodes);
        // create the NodeExtentProvider for TextInBox nodes
        TextInBoxNodeExtentProvider nodeExtentProvider = new TextInBoxNodeExtentProvider();

        // create the layout
        TreeLayout<TextInBox> treeLayout = new TreeLayout<TextInBox>(tree,
                nodeExtentProvider, configuration);
        // Create a panel that draws the nodes and edges and show the panel
        TextInBoxTreePane panel = new TextInBoxTreePane(treeLayout);
        showInDialog(panel);
        }
    }

    private static void showInDialog(JComponent panel) {
        /*JDialog dialog = new JDialog();
        Container contentPane = dialog.getContentPane();
        ((JComponent) contentPane).setBorder(BorderFactory.createEmptyBorder(
                10, 10, 10, 10));
        contentPane.add(panel);*/
        /*dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        dialog.pack();
        dialog.setLocationRelativeTo(null);
        dialog.setVisible(true);
*/
        JFrame mainFrame = new JFrame("Tree parsing");
        Container contentPane = mainFrame.getContentPane();
        contentPane.add(panel);
        mainFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        mainFrame.setPreferredSize(new Dimension(400, 600));
        mainFrame.pack();
        mainFrame.setVisible(true);

    }

	public GUI getGui() {
		return gui;
	}

	public void setGui(GUI gui) {
		this.gui = gui;
	}
}
