package it.uniroma2.informatica.parser;

import it.uniroma2.informatica.domain.Query;
import it.uniroma2.informatica.domain.UserInput;
import it.uniroma2.informatica.exceptions.EmptyStringException;
import it.uniroma2.informatica.exceptions.InternalParserException;
import it.uniroma2.informatica.exceptions.NotWellFormattedString;
import it.uniroma2.informatica.exceptions.NullStringException;

import java.util.List;

/**
 * Gestore per il parsing dell'input dell'utente. 
 * Il gestore si occupa di trasformare l'input dell'untente in una query.
 * Created by Valerio Martella on 18/12/2014.
 */
public interface ParserManager {

    public List<Query> parseInput(UserInput userInput) throws NullStringException, EmptyStringException, NotWellFormattedString, InternalParserException;

}