package it.uniroma2.informatica.parser;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import it.uniroma2.informatica.domain.Query;
import it.uniroma2.informatica.domain.Resource;
import it.uniroma2.informatica.domain.Triple;
import it.uniroma2.informatica.domain.TripleElement;
import it.uniroma2.informatica.domain.UserInput;
import it.uniroma2.informatica.domain.Variable;
import it.uniroma2.informatica.exceptions.EmptyStringException;
import it.uniroma2.informatica.exceptions.NotWellFormattedString;
import it.uniroma2.informatica.exceptions.NullStringException;
import it.uniroma2.informatica.uimanager.UIManager;
import it.uniroma2.informatica.utils.Preconditions;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A partire da un template in XML, contenente pattern che rappresentano espressioni regolari,
 * naviga l'albero e restituisce gli item di ogni tag <pattern></pattern>. La struttura del template
 * in input deve seguire il seguente scheletro:
 * - <template> </template> è il tag radice i cui figli sono liste di
 * - <pattern> </pattern> che contenono stringhe rappresentanti le espressioni regolari.
 * Created by danielepasquini on 18/12/14.
 */
public class TemplateXmlParser implements ParserManager {

    DocumentBuilder mBuilder;
    Document mDocument;

    public TemplateXmlParser() {
        startXmlParser();
    }
    
    /**
     * La classe prende un input dall'utente in linguaggio naturale e lo confronta 
     * con dei pattern specificati nel file src/main/resources/template.xml
     * che devono avere dei campi #S, #P e #O.  
     * Se la frase matcha con qualche template il metodo restituisce 
     * un oggetto di tipo query: 
     * con predicato corrispondente alla parte del template #P
     * con oggetto corrispondente alla parte del template #O 
     * se il soggetto non e' presente viene impostato come una variabile
     * altrimenti con soggetto corrispondente alla parte del template #S
     * @param userInput input dell'utente
     * @return query 
     */
    public List<Query> parseInput (UserInput userInput) throws NullStringException, EmptyStringException, NotWellFormattedString
    {
    	List<Query> listQuery = new ArrayList<Query>();
    	
    	//Preconditions
    	Preconditions.checkNotNull(userInput.getmInput(), "User Input e' null");
    	Preconditions.checkState(userInput.getmInput().length() > 0, "Input e 'null");

		//Trim String
		userInput.setmInput(userInput.getmInput().trim());

        //La stringa viene divisa in un ArrayList che contiene le singole parole
    	//ArrayList<String> question = new ArrayList<String>(Arrays.asList(userInput.getmInput().split(" |\\?|\\'")));

        if(userInput.getmInput() == null)
            throw new NullStringException();

        if(userInput.getmInput().length() == 0)
            throw new EmptyStringException();
        
    	TripleElement mSubject = new Variable();
    	TripleElement mPredicate = new Variable();
    	TripleElement mObject = new Variable(); 
    	
    	int mSubject_index;
    	int mPredicate_index;
    	int mObject_index;

    	/*
    	 * Lista di pattern ricavati dal file di configurazione src/main/resources/template.xml
    	 */
    	
    	TemplateXmlParser mXmlParser = new TemplateXmlParser();
    	List<String> mTemplates = mXmlParser.parseTemplate();
    	
    	Pattern mPattern;
    	Matcher mMatcher;
    	
    	//espressione regolare per rilevare le stringhe 
    	String wildcard = "([a-zA-Z -]+)";
    	
    	boolean patternMatch = false;
    	
    	for(String pattern: mTemplates)
    	{
    		/*
    		 * i caratteri speciali che identificano soggetto ,predicato ed oggetto 
    		 * vengono trasformati in espressioni regolari che ammettano sequenze di parole
    		 * e si cerca un match con la frase di input.
    		 */
    		String regex = pattern.replaceAll("#P|#O|#S", wildcard);
    		
    		mPattern = Pattern.compile(regex,Pattern.CASE_INSENSITIVE);
    		mMatcher = mPattern.matcher(userInput.getmInput().replaceAll("( )+"," "));
    		
    		/*
    		 * Se la stringa di input matcha il pattern allora vengono estratti 
    		 * Oggetto, Predicato e se presente nel pattern Soggetto
    		 */
    		if(mMatcher.matches())
    		{
    			patternMatch = true;
				UIManager.getIstance().getGui().addLog(pattern);
    			System.out.println(pattern);
    			
    			ArrayList<String> structure = new ArrayList<String>();
    			int gap = 0;
    			int[] groupsInBetween = new int[3];
    			
    			/*
    			 * si estrae una rappresentazione del pattern per dedurre le posizioni relative di 
    			 * #O,#P e #S se presente, e si calcolano i gruppi tra di loro
    			 */
    			for(int i=0;i< pattern.length()-1; i++)
    			{
    				if(pattern.substring(i, i+1).equals("("))
    				{
    					groupsInBetween[gap]++; 
    				}
    				else
    					if(pattern.substring(i, i+2).matches("#[SOP]"))
    					{
    						structure.add(pattern.substring(i, i+2));
    						gap++;
    					}
    			}
    			
    			/*
    			 * Si calcola il gruppo corrispondente all'elemento di tripla desiderato,
    			 * lo si estrae e si crea la risorsa da assegnare alla tripla.
    			 */
    			mObject_index = structure.indexOf("#O")+1; // il +1 serve a far coincidere gli indici
    			for(int i=0;i<=structure.indexOf("#O");i++)
    			{
    				mObject_index += groupsInBetween[i];   //si aggiungono i gruppi non derivati dalle wildcard sostituite
    			}
    			String obj = mMatcher.group(mObject_index);
    			mObject = new Resource(obj);
    			
    			mPredicate_index = structure.indexOf("#P")+1;
    			for(int i=0;i<=structure.indexOf("#P");i++)
    			{
    				mPredicate_index += groupsInBetween[i]; 
    			}
    			mPredicate = new Resource(mMatcher.group(mPredicate_index));
    			
    			if(pattern.contains("#S"))
    			{
    				mSubject_index = structure.indexOf("#S")+1;
    				for(int i=0;i<=structure.indexOf("#S");i++)
        			{
        				mSubject_index += groupsInBetween[i]; 
        			}
        			mSubject = new Resource(mMatcher.group(mSubject_index));
    			}
    			
    			/*
    			 * il break garantisce che solo il primo pattern che corrisponde venga esaminato
    			 */
    			break;
    		}
    	}
    	
    	/*
    	 * Se nessun pattern corrisponde la stringa non � interpretabile 
    	 * e viene notificato al livello superiore
    	 */
    	if(patternMatch == false)
    	{
    		throw new NotWellFormattedString();
    	}
    	
    	/*
    	 * 
    	 */
    	//listQuery.add(new Query(new Triple(mSubject, mPredicate, mObject)));
		listQuery.add(new Query(new Triple(mSubject, mPredicate, mObject)));
    	
        return listQuery;
    }


    /**
     * Istanzia il Builder e il Document per effettuare il parsing su un file di default
     */
    public void startXmlParser() {
        try {
            mBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            mDocument = mBuilder.parse(new File("src/main/resources/template.xml"));
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    /**
     * Effettua il parsing del template XML restituendo gli elementi contenuti
     * in ogni tag <pattern></pattern>
     * @return lista di pattern
     */
    public List<String> parseTemplate() {
        XPath xPath = XPathFactory.newInstance().newXPath();
        //Query per file XML: restituisce tutti i pattern del template
        // identificati dal tag <pattern>
        String expression = "/template/pattern/text()";

        try {
            NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(mDocument, XPathConstants.NODESET);
            if(nodeList != null) {

                List<String> pattern = new ArrayList<String>();
                for (int i = 0; i < nodeList.getLength(); i++) {
                    // Nodo Pattern
                    Node node = nodeList.item(i);
                    pattern.add(node.getTextContent());
                }
                return pattern;
            }
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
        return null;
    }

}
