package it.uniroma2.informatica.parser;

import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpParser;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpSegmenter;
import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.fit.factory.AnalysisEngineFactory;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;

/**
 * Created by danielepasquini on 01/02/15.
 *
 * Classe Helper per l'istanziazione di uimaFIT, la creazione degli engine per il parsing delle stringhe (segmenter,
 * parser) e il contenitore JCas.
 * Si tratta di una classe singleton, nata per limitare la creazione di troppe istanze, a causa
 * della lentezza nella creazione e nell'avvio degli engine.
 */
public class UimaHelper {

    // Singleton object of the class
    private static UimaHelper sUimaHelper = null;

    private AnalysisEngine mSegmenter = null;
    private AnalysisEngine mParser = null;
    private JCas mUimaContainer;


    private UimaHelper() {
        sUimaHelper = this;
        createUimaFitEngine();
    }

    /**
     *
     * @return una nuova istanza della classe UimaHelper nel caso non sia stata ancora istanziata
     * altrimenti ritorna l'oggetto già istanziato
     */
    public static UimaHelper getInstance() {
        if(sUimaHelper==null) {
            sUimaHelper = new UimaHelper();
        }
        return sUimaHelper;
    }


    /**
     * Crea il segmenter, il parser e il JCas tramite UIMA, se ancora non creati
     */
    private void createUimaFitEngine() {
        //TODO Cambiare il tipo di return in querylist. Rivedere il configuration manager per la tipologia di parser
        if(mParser == null || mSegmenter == null) {
            try {
                // Istanzia il segmenter per tokenizzare la frase in input
                mSegmenter = AnalysisEngineFactory.createEngine(OpenNlpSegmenter.class);
                // Istanzia il parser
                mParser = AnalysisEngineFactory.createEngine(OpenNlpParser.class, OpenNlpParser.PARAM_WRITE_PENN_TREE, true);
                // Crea il JCas
                mUimaContainer = JCasFactory.createJCas();
            } catch (ResourceInitializationException e) {
                e.printStackTrace();
            } catch (UIMAException e) {
                e.printStackTrace();
            }
        }
    }

    public AnalysisEngine getSegmenter() {
        return mSegmenter;
    }

    public AnalysisEngine getParser() {
        return mParser;
    }

    public JCas getContainer() {
        return mUimaContainer;
    }
}
