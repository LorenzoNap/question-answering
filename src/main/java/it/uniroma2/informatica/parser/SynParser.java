package it.uniroma2.informatica.parser;

import it.uniroma2.informatica.domain.BinaryTree;
import it.uniroma2.informatica.domain.Node;
import it.uniroma2.informatica.domain.NonTerminal;
import it.uniroma2.informatica.domain.Query;
import it.uniroma2.informatica.domain.Resource;
import it.uniroma2.informatica.domain.Terminal;
import it.uniroma2.informatica.domain.Triple;
import it.uniroma2.informatica.domain.TripleElement;
import it.uniroma2.informatica.domain.UserInput;
import it.uniroma2.informatica.domain.Variable;
import it.uniroma2.informatica.exceptions.EmptyStringException;
import it.uniroma2.informatica.exceptions.InternalParserException;
import it.uniroma2.informatica.exceptions.NotWellFormattedString;
import it.uniroma2.informatica.exceptions.NullStringException;
import it.uniroma2.informatica.uimanager.TextInBox;
import it.uniroma2.informatica.uimanager.UIManager;
import it.uniroma2.informatica.utils.Preconditions;
import it.uniroma2.informatica.utils.SystemConstants;

import java.util.ArrayList;
import java.util.List;

import org.abego.treelayout.util.DefaultTreeForTreeLayout;
import org.apache.log4j.Logger;
import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.tcas.Annotation;

import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import de.tudarmstadt.ukp.dkpro.core.api.syntax.type.constituent.Constituent;
import de.tudarmstadt.ukp.dkpro.core.api.syntax.type.constituent.SBARQ;
import de.tudarmstadt.ukp.dkpro.core.api.syntax.type.constituent.SQ;

/**
 * Created by danielepasquini on 16/01/15.
 */
public class SynParser implements ParserManager{
    static Logger log = Logger.getLogger(SynParser.class.getName());
    static boolean D = true;

    private BinaryTree mBinaryTree;
    private List<Query> mQueryList;


    /**
     * Esegue il parsing, incapsulato all'interno di UIMA
     *
     * @param userInput
     * @throws NullStringException
     * @throws EmptyStringException
     * @throws NotWellFormattedString
     * @throws UIMAException
     */
    public List<Query> parseInput(UserInput userInput) throws NullStringException, EmptyStringException, NotWellFormattedString, InternalParserException{
        Preconditions.checkNotNull(userInput);

        UIManager.getIstance().getGui().addLog("Loading uimaFIT");
        JCas uimaContainer = UimaHelper.getInstance().getContainer();
        // Reset JCas per effettuare il parsing di un nuovo input
        uimaContainer.reset();
        UIManager.getIstance().getGui().addLog("end loading uimaFIT");

        uimaContainer.setDocumentText(userInput.getmInput());
        uimaContainer.setDocumentLanguage(SystemConstants.ENGLISH);

        // Launch an internal parser exception.
        try {
			SimplePipeline.runPipeline(uimaContainer, UimaHelper.getInstance().getSegmenter(), UimaHelper.getInstance().getParser());
		} catch (AnalysisEngineProcessException e) {
			// TODO Auto-generated catch block
			throw new InternalParserException("UIMA Exception");
		}
        analyzeQuestionInput(uimaContainer);
        mQueryList = new ArrayList<Query>();
        fillQueryList();
        return mQueryList;
    }

    /**
     * Riconosce il tipo di albero (SBARQL e SQ) basandosi sulla struttura dell'albero
     * (L'albero SBARQL ha un nodo aggiuntivo come radice, mentre l'albero SQ no) e
     * riempie la lista di Query conseguentemente
     */
    private void fillQueryList () {
        //Caso SBARQL
        if(mBinaryTree.getRoot().getChildren().size() == 1)
            queryBuilder(mBinaryTree.getRoot().getChildren().get(0), new Variable());
        //Caso SQ: utilizzo il predicato del primo livello come soggetto del secondo livello, per rispecchiare la struttura della domanda
        else {
            queryBuilder(mBinaryTree.getRoot(), new Variable());
            Query tmpQuery = mQueryList.remove(0);
            mQueryList.get(0).getmTriple().setmSubject(new Resource(tmpQuery.getmTriple().getmPredicate().getRepresentation()));
        }
        for (Query e : mQueryList)
            log.info("Soggetto: " + e.getmTriple().getmSubject().getRepresentation() +
                    " Predicato: " + e.getmTriple().getmPredicate().getRepresentation()
                    + " Oggetto " + e.getmTriple().getmObject().getRepresentation());

    }

    /*private List<Query> queryBuilder(BinaryTree binaryTree){
        ArrayList<Query> queryList = new ArrayList<Query>();
        int i = 0;
        Queue<Node> queue = new LinkedList<Node>();
        queue.add(binaryTree.getRoot());

        while (!queue.isEmpty()){
            Node tmp = queue.remove();
            if(tmp instanceof NonTerminal){
                if((tmp.getChildren().get(0) instanceof Terminal)&&(tmp.getChildren().get(1) instanceof Terminal))
                    {
                        Query mQuery = new Query(new Triple());
                    }
                if(){}
                if(){}
                if(){}
                Query mQuery = new Query(new Triple(, , new Variable()));
            }
        }

        return queryList;
    }*/


    /**
     * Crea la query sfruttando la struttura dell'albero
     * Il soggetto è sempre una variabile
     * Il figlio sinistro rappresenta il predicato
     * Il figlio destro rappresenta l'oggetto
     * Se il nodo in questione è un terminale, l'elemento diventa una Resource con il valore del terminale
     * Altrimenti diventa una variabile
     *
     * @param node
     * @param variable
     */
    private void queryBuilder(Node node, TripleElement variable){
        if(node.getChildren().size()!=0){
            log.info("Il nodo " + node.getData() + " ha figli ");
            for (Node j : node.getChildren())
                log.info(j.getData());
            mQueryList.add(compareNode(node, (Variable) variable));
            for(Node j : node.getChildren()) {
                queryBuilder(j, mQueryList.get(mQueryList.size()-1).getmTriple().getmObject());
            }
        }
    }

    /**
     * Crea la Query basandosi sulla natura dei figli (Terminali o Non Terminali)
     * Il soggetto viene passato come parametro per mantenere il legame fra le query
     * Predicato e Oggetto vengono istanziati
     * come Resource se i figli sono Terminali
     * come Oggetto se i figli sono Non Terminali
     *
     * @param node
     * @param variable
     * @return
     */
    private Query compareNode(Node node, Variable variable){
        Variable subject = variable;
        TripleElement predicate = null;
        TripleElement object = null;
        //Caso Terminale-Terminale
        if(!(node.getChildren().get(0).getData().equals("NP"))&&!(node.getChildren().get(1).getData().equals("NP")))
        {
            //log.info("T - T");
            predicate = new Resource(node.getChildren().get(0).getData());
            object = new Resource(node.getChildren().get(1).getData());
        }
        //Caso Non Terminale-Terminale
        if((node.getChildren().get(0).getData().equals("NP"))&&!(node.getChildren().get(1).getData().equals("NP")))
        {
            //log.info("N - T");
            predicate = new Variable();
            object = new Resource(node.getChildren().get(1).getData());
        }
        //Caso Terminale-Non Terminale
        if(!(node.getChildren().get(0).getData().equals("NP"))&&(node.getChildren().get(1).getData().equals("NP")))
        {
            //log.info("T - n");
            predicate = new Resource(node.getChildren().get(0).getData());
            object = new Variable();
        }
        //Caso Non Terminale-Non Terminale
        if((node.getChildren().get(0).getData().equals("NP"))&&(node.getChildren().get(1).getData().equals("NP")))
        {
            //log.info("N - n");
            predicate = new Variable();
            object = new Variable();
        }
        return new Query(new Triple(subject, predicate, object));
    }

    /**
     * Estrae i dati dal JCas e costruisce l'albero di parsing
     * a partire dal nodo radice S.
     *
     * @param jcas
     */
    public void analyzeQuestionInput(JCas jcas) throws NotWellFormattedString {
        Preconditions.checkNotNull(jcas);
        boolean isQuestionInput = false;

        FSIterator<Annotation> iterAnn = jcas.getAnnotationIndex().iterator();
        while (iterAnn.hasNext()) {
            Annotation annotation = iterAnn.next();
            if (annotation instanceof SBARQ || annotation instanceof SQ) {
                isQuestionInput = true;

                if(D) log.info("Annotation è un simbolo terminale di tipo SBARQ");

                // Istanzia l'albero di parsing, passando S come radice dello stesso
                // con una lista vuota di figli ad esso collegata
                mBinaryTree = new BinaryTree(new Node(SystemConstants.SBARQ, new ArrayList<Node>(), null));
                if(D) log.info("BinaryTree instanziato con nodo radice SBARQ");

                // Visita l'albero prodotto da UIMA creando parallelamente l'albero di parsing
                treeVisit(annotation, mBinaryTree.getRoot());
                break;
            }
            //TODO ritornare un messaggio d'errore nel caso non ci sia la domanda
        }
        //Fai un print dell'albero
        printTreeForGUI();

        if(isQuestionInput == true) {
            removeNode(mBinaryTree.getRoot());
            printTree(mBinaryTree.getRoot());
        } else {
            throw new NotWellFormattedString();
        }
    }

    /**
     * Esegue ricorsivamente la visita dell'albero di parsing di UIMA e
     * costruisce l'albero di parsing BinaryTree.
     *
     * @param nodeSpanningTree Nodo dell'albero di parsing UIMA
     * @param nodeBinaryTree Nodo dell'albero di parsing, struttura creata ad-hoc, BinaryTree
     */
    private void treeVisit(Annotation nodeSpanningTree, Node nodeBinaryTree) {
        if(D) log.info("Visita albero ricorsiva");
        Preconditions.checkNotNull(nodeSpanningTree);
        Preconditions.checkNotNull(nodeBinaryTree);

        // Figli di nodeSpanningTree
        FSArray childrenNodeSpanningTree = ((Constituent) nodeSpanningTree).getChildren();
        // Se il nodo non è foglia
        if (childrenNodeSpanningTree.size() != 0) {
            // Per ogni figlio
            for (int i = 0; i < childrenNodeSpanningTree.size(); ++i) {
                // Nodo figlio di nodeSpanningTree in esame
                Annotation childNodeSpanningTree = ((Annotation) childrenNodeSpanningTree.get(i));
                if(D) log.info("Tipologia nodo: " + childNodeSpanningTree.getClass().getSimpleName());

                // Se non è un preterminale
                if (!(childNodeSpanningTree instanceof Token)) {

                    // Crea un nuovo nodo NonTerminal e lo aggiunge all'albero di parsing BinaryTree
                    NonTerminal newChildNodeBinaryTree = new NonTerminal(childNodeSpanningTree.getClass().getSimpleName(), new ArrayList<Node>(), nodeBinaryTree);
                    nodeBinaryTree.getChildren().add(newChildNodeBinaryTree);
                    if(D) log.info("NonTerminal: nodo Non Terminale aggiunto a BinaryTree: " + newChildNodeBinaryTree.getData());

                    // Esegue ricorsivamente la visita
                    treeVisit(childNodeSpanningTree, newChildNodeBinaryTree);
                } else {
                    // Essendo un Token, instanzia una stringa con il part of speech
                    String partOfSpeech = ((Token) childNodeSpanningTree).getPos().getPosValue();
                    if(D) log.info("Nodo PoS Token, partOfSpeech: " + partOfSpeech + " coveredText: " + childNodeSpanningTree.getCoveredText());

                    // Controlla se il part of speech della Penn Treebank è un
                    // - NN = Nome singolare
                    // - NNS = Nome plurale
                    // - NNP = Nome proprio singolare
                    // - NNPS = Nome proprio plurale
                    if (partOfSpeech.equals(SystemConstants.NN) ||
                            partOfSpeech.equals(SystemConstants.NNS) ||
                            partOfSpeech.equals(SystemConstants.NNP) ||
                            partOfSpeech.equals(SystemConstants.NNPS)) {

                        // Crea un nuovo nodo Terminal e lo aggiunge all'albero di parsing BinaryTree
                        Terminal newChildNodeBinaryTree = new Terminal(partOfSpeech, new ArrayList<Node>(), nodeBinaryTree);
                        nodeBinaryTree.getChildren().add(newChildNodeBinaryTree);

                        // Setta il valore "data" del nodo con la stringa coperta dal preterminale
                        nodeBinaryTree.getChildren().get(nodeBinaryTree.getChildren().
                                lastIndexOf(newChildNodeBinaryTree)).
                                setData(childNodeSpanningTree.getCoveredText());
                        if(D) log.info("Terminal: nodo PoS Token aggiunto a BinaryTree: " + newChildNodeBinaryTree.getData());
                    }
                }
            }

        }
    }

    /**
     * Rimuove i nodi non utili all'elaborazione della query dall'albero
     * @param node
     */
    private void removeNode(Node node) {
       // log.info("Analizziamo il nodo " + node.getData());
        //Passo base: controllo se siamo in una foglia o no
        if (node.getChildren().size() != 0)
            //controllo tutti i figli del nodo che stiamo analizzando
            for (Node i : node.getChildren()) {
                //Se uno dei figli è un nodo da eliminare
                if (!(i.getData().equals("NP") || i instanceof Terminal)) {
                    //log.error("entrato nel nodo " + i.getData());
                    //Prendo tutti i figli di questo nodo
                    for (Node j : i.getChildren()) {
                        //log.info("x: Analizziamo il nodo contenente" + j.getData());
                        //per ognuno di questi figli cambio il padre e lo setto con il nodo che stiamo analizzando
                        j.setParent(i.getParent());
                        i.getParent().getChildren().add(j);
                    }
                    //log.info("rimuoviamo" + i.getData() + " da " + i.getParent().getData());
                    //rimuovo il nodo inutile dalla lista dei figli del nodo che stiamo analizzando
                    Node tmpNode = i.getParent();
                    tmpNode.getChildren().remove(i);
                    //riparto con il passo ricorsivo
                    removeNode(tmpNode);
                    break;
                }
                /*for(Node j : i.getChildren())
                        log.info("Il nodo" + i.getData() + "ha come figlio" + j.getData());
                if(i instanceof NonTerminal)
                    log.info("NonTerminal: " +  i.getData());
                else log.info("Terminal: " +  i.getData());*/
                //se siamo in un nodo da tenere continuiamo la visita
                removeNode(i);
            }
    }

    /**
     * Assegno ai nodi NP che hanno uno o più terminali come figli il valore di questi ultimi opportunamente formattati
     *
     * @param node
     */
    private void printTree(Node node) {
        //Creo una stringa vuota
        String replace = new String();
        //Controllo se il nodo ha almeno un figlio terminale
        for(Node i :node.getChildren())
            if (i instanceof Terminal)
                //aggiungo alla stringa il nome estratto
                replace += "_" + i.getData();

        //Se la stringa non è vuota rendo il nodo NP un terminale con la stringa
        if(replace.length()!=0) {
            node.setData(replace.substring(1));
            node.setChildren(new ArrayList<Node>());
        }
        log.info("Stampiamo il nodo " + node.getData() + " che ha come figli ");
        for (Node i : node.getChildren())
        log.info(i.getData());
        if (node.getChildren().size() != 0)
            for (Node i : node.getChildren()) {
               /* if(i instanceof NonTerminal)
                    log.info("NonTerminal: " +  i.getData());
                else log.info("Terminal: " +  i.getData());
                */printTree(i);
            }
    }

    private void printTreeForGUI(){
        UIManager.getIstance().printGUITree(mBinaryTree);
    }


}
