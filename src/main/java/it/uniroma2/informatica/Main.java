package it.uniroma2.informatica;


import it.uniroma2.informatica.systemController.SystemController;

import org.apache.log4j.Logger;

/**
 * Created by Giovanni Lorenzo Napoleoni on 13/12/2014.
 */
public class Main {
    static Logger log = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {

        log.info("Start Question Answering");
        SystemController systemController = new SystemController();
        systemController.getUiManager().createGUI(systemController);
    }

}
