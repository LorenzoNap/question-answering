package it.uniroma2.informatica.synonymer;

import it.uniroma2.informatica.domain.Resource;
import it.uniroma2.informatica.uimanager.UIManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import edu.smu.tspell.wordnet.NounSynset;
import edu.smu.tspell.wordnet.Synset;
import edu.smu.tspell.wordnet.SynsetType;
import edu.smu.tspell.wordnet.WordNetDatabase;

/**
 * Gestore per il recupero dei sinonimi a partire da una query. I sinonimi vengono resituiti sono sotto forma di query per una successiva ricereca all'interno dell'ontologia.
 * Created by Giovanni Lorenzo Napoleoni on 13/12/2014.
 */
public class SynonymerManager {

	static Logger log = Logger.getLogger(UIManager.class.getName());

	private WordNetDatabase database;

	public SynonymerManager(){
		database = WordNetDatabase.getFileInstance();
	}


	/**
	 * 
	 * @param word
	 * @return List of noun synsets for the given word.
	 */
	private List<NounSynset> getSynsetsFromWord(String word) {

		Synset[] synsets = database.getSynsets(word, SynsetType.NOUN);
		List<NounSynset> listNounSynset = new ArrayList<NounSynset>();
		for (Synset synset : synsets){
			if(synset instanceof NounSynset){
				listNounSynset.add((NounSynset) synset);
			}
		}
		return listNounSynset;

	}

	/**
	 * 
	 * @param nounSynsets
	 * @return lista di parole all'interno dei vari synset
	 */
	private List<String> getWordsSynsets(List<NounSynset> nounSynsets){
		List<String> listSynonyms = new ArrayList<String>();
		for (NounSynset nounSynset : nounSynsets){
			for(String word : nounSynset.getWordForms()){
				listSynonyms.add(word);
			}
		}
		return listSynonyms;	    	
	}


	/**
	 * Data una query, trova i sinonimi per gli elementi contenuti all'interno della query
	 * @param query query
	 * @return lista di query contenente i sinonimi trovati.
	 */
	public List<String> fetchSynonyms(Resource resource){
		{
			// Usa l'API JAWS per interrogare Wordnet
			String entry = resource.getmValue();

			List<NounSynset> synsets = getSynsetsFromWord(entry);

			List<String> synonyms = getWordsSynsets(synsets);

			log.info("Sinonimi: " + synonyms);

			return synonyms;
		}
	}


	/**
	 * 
	 * @param resourcePredicate
	 * @param depthLevel
	 * @return List of hyponyms at a specified depth level.
	 */

	public List<String> fetchHyponyms(Resource resource,
			Integer depthLevel) {
		String entry = resource.getmValue();
		List<NounSynset> listSynsets = new ArrayList<NounSynset>();
		listSynsets = getSynsetsFromWord(entry);

		List<String> listHyponyms = new ArrayList<String>();
		
		// Go to the specified depth level.
		for (int i = 1; i <= depthLevel; i++) {
			List<NounSynset> listSynsetHyponyms = new ArrayList<NounSynset>();

			for (NounSynset synset : listSynsets){
				NounSynset[] hyponyms;
				hyponyms = synset.getHyponyms();
				listSynsetHyponyms.addAll(Arrays.asList(hyponyms));
			}

			// Remove previous level of synsets
			listSynsets.clear();
			// Update the list of synsets related to the current depth level.
			listSynsets.addAll(listSynsetHyponyms);
		}

		// Reached depth level k.
		listHyponyms.addAll(getWordsSynsets(listSynsets));

		log.info("Hyponym : " + listHyponyms + " at depth " + depthLevel);
		return listHyponyms;
	}

	/**
	 * 
	 * @param resourcePredicate
	 * @param depthLevel
	 * @return  List of hypernyms at a specified height level.
	 */

	public List<String> fetchHypernyms(Resource resource,
			Integer heightLevel) {
		String entry = resource.getmValue();
		List<NounSynset> listSynsets = new ArrayList<NounSynset>();
		listSynsets = getSynsetsFromWord(entry);

		List<String> listHypernyms = new ArrayList<String>();
		
		// Go to the specified depth level.
		for (int i = 1; i <= heightLevel; i++) {
			List<NounSynset> listSynsetHypernyms = new ArrayList<NounSynset>();

			for (NounSynset synset : listSynsets){
				NounSynset[] hypernyms;
				hypernyms = synset.getHypernyms();
				listSynsetHypernyms.addAll(Arrays.asList(hypernyms));
			}

			// Remove previous level of synsets
			listSynsets.clear();
			// Update the list of synsets related to the current depth level.
			listSynsets.addAll(listSynsetHypernyms);
		}

		// Reached depth level k.
		listHypernyms.addAll(getWordsSynsets(listSynsets));

		log.info("Hypernym : " + listHypernyms + " at height " + heightLevel);
		return listHypernyms;
	}
}
