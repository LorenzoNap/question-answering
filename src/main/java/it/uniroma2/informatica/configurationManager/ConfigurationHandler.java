package it.uniroma2.informatica.configurationManager;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Properties;
/**
 * 
 * @author rob
 *
 */

public class ConfigurationHandler {
	
	private static ConfigurationHandler configurationHandler = null;
	private static String sPropFileName = null;  //nome del file di properties
	
	//Elenco di variabili relative ai moduli, contenute nel file .properties
	private static String WordnetPath = null; 
	
	public String getWordnetPath() {
		System.out.println("Path: " + WordnetPath);
		return WordnetPath;
	}
	public void setWordnetPath(String wordnetPath) {
		WordnetPath = wordnetPath;
	}

	private static String TemplateParser = null;
	private static String SyntacticParser = null;
	private static String Synonimer = null;
	private static String VisualizeResultToUser = null;
	private static Integer MaxHyponymDepthLevel = null;
	private static Integer MaxHypernymHeightLevel = null;

	
	/**
	 * Costruttore private, richiesto dal pattern Singleton
	 */
	private ConfigurationHandler(){}
	/**
	 * getter del Singleton, dopo la creazione dell'oggetto singoletto chiama updateValues() per aggiornare sul
	 * singoletto i valori del file .properties
	 * 
	 * @param propFileName
	 * @return ConfigurationHandler
	 * @throws java.io.IOException
	 */
	public static synchronized ConfigurationHandler getConfigurationHandler(String propFileName) throws IOException{
		sPropFileName= propFileName;
		if (configurationHandler == null) {
			configurationHandler = new ConfigurationHandler();
		}
		sPropFileName= propFileName;
		configurationHandler.updateValues();
        return configurationHandler;
	}
	
	/**
	 * getter che ritorna il valore relativo alla costante del modulo SyntacticParser : true o false
	 * @return SyntacticParser
	 */
	
	public String getSyntacticParser() {
		return SyntacticParser;
	}
	
	/**
	 * getter che ritorna il valore relativo alla costante del modulo TemplateParser : true o false
	 * @return TemplateParser
	 */
	public String getTemplateParser() {
		return TemplateParser;
	}
	

	/**
	 * getter che ritorna il valore relativo alla costante del modulo Synonimer : true o false
	 * @return Synonimer
	 */
	public String getSynonimer() {
		return Synonimer;
	}
	/**
	 * getter che ritorna il valore relativo alla costante del modulo VisualizeResultToUser : true o false
	 * @return VisualizeResultToUser
	 */
	
	public String getVisualizeResultToUser() {
		return VisualizeResultToUser;
	}
	
	/**
	 * getter che ritorna il valore relativo alla costante del modulo SaveResultsOnFile : true o false
	 * @return SaveResultsOnFile
	 */
	public Integer getMaxHyponymDepthLevel() {
		return MaxHyponymDepthLevel;
	}
	
	/**
	 * getter che ritorna il valore relativo alla costante del modulo SaveResultsOnFile : true o false
	 * @return SaveResultsOnFile
	 */
	public Integer getMaxHypernymHeightLevel() {
		return MaxHypernymHeightLevel;
	}
/**
 * Aggiorna i valori del sistema sul singleton: estrae le costanti relative ai moduli da attivare o meno e le memorizza su variabili statiche
 * @throws java.io.IOException
 */
	public void updateValues() throws IOException{
		Properties prop = new Properties();
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(sPropFileName);

		if (inputStream != null) {
			prop.load(inputStream);
		}
		else {
			throw new FileNotFoundException("Il file .property '" + sPropFileName + "' non è stato trovato nel classpath");
		}	
        WordnetPath = prop.getProperty("wordnet.dictonary.path");
		TemplateParser = prop.getProperty("TemplateParser");
		SyntacticParser = prop.getProperty("SyntacticParser");
		Synonimer = prop.getProperty("Synonimer");
		VisualizeResultToUser = prop.getProperty("VisualizeResultToUser");
		MaxHyponymDepthLevel = Integer.decode(prop.getProperty("MaxHyponymDepthLevel"));
		MaxHypernymHeightLevel = Integer.decode(prop.getProperty("MaxHypernymHeightLevel"));

	}
		
	//
	/**
	 *  Restituisce lo stato d'uso globale di tutte le impostazioni tramite un ArrayList di String
	 * @return statusWord
	 * @throws java.io.IOException
	 */
	
	public ArrayList<String> getAllPropValues() throws IOException {

		ArrayList<String> statusWord = new ArrayList<String>();
			
		//Inserimento delle costanti nell'array list
		statusWord.add(WordnetPath);
		statusWord.add(TemplateParser);
		statusWord.add(SyntacticParser);
		statusWord.add(Synonimer);
		statusWord.add(VisualizeResultToUser);
						
		return statusWord;
	}

/**
 * Ritorna il nome del file .properties attualmente impostato
 * @return sPropFileName
 */
	public  String getConfigFileName(){
		return sPropFileName;
	}
	
/**
 * Setta il nome del file .properties, null di default
 * @param propFileName
 */
	
	public void setConfigFileName(String propFileName){
		sPropFileName = propFileName;
	}
	
	public void trim(){
		// TODO pulisce la stringa da errori di battitura: semplice controllo isTrue isFalse
	}

	public static void setVisualizeResultToUser(String visualizeResultToUser) {
		VisualizeResultToUser = visualizeResultToUser;
	}

	public static void setSynonimer(String synonimer) {
		Synonimer = synonimer;
	}

	public static void setSyntacticParser(String syntacticParser) {
		SyntacticParser = syntacticParser;
	}

	public static void setTemplateParser(String templateParser) {
		TemplateParser = templateParser;
	}

	public static void setsPropFileName(String sPropFileName) {
		ConfigurationHandler.sPropFileName = sPropFileName;
	}

	public static void setConfigurationHandler(ConfigurationHandler configurationHandler) {
		ConfigurationHandler.configurationHandler = configurationHandler;
	}
}

