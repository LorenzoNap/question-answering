package it.uniroma2.informatica.exceptions;

public class InternalParserException extends Exception {
	private static final long serialVersionUID = 9111635184819468188L;

	public InternalParserException() {
        super("Internal parser exception");
    }
	
	public InternalParserException(String exception){
		super(exception);
	}
}
