package it.uniroma2.informatica.exceptions;

public class NullStringException extends Exception {
	private static final long serialVersionUID = 612795804763658804L;

	public NullStringException() {
        super("Stringa nulla");
    }
}
