package it.uniroma2.informatica.exceptions;

public class NotWellFormattedString extends Exception {
	private static final long serialVersionUID = 8783224789325590712L;

	public NotWellFormattedString() {
        super("La stringa non e' formattata secondo i template proposti ");
    }
}
