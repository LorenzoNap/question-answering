package it.uniroma2.informatica.exceptions;

public class EmptyStringException extends Exception {
	private static final long serialVersionUID = 9111635184819468188L;

	public EmptyStringException() {
        super("Stringa in input vuota");
    }
}
