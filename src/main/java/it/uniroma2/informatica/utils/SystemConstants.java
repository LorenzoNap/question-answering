package it.uniroma2.informatica.utils;

/**
 * Created by Giovanni Lorenzo Napoleoni on 16/12/2014.
 */
public class SystemConstants {

    public static final String DBPEDIA_ENDPOINT = "http://dbpedia.org/sparql";

    public static final String DBPEDIA_URI_PROPERTY = "http://dbpedia.org/property/";

    public static final String DBPEDIA_URI_RESOURCE = "http://dbpedia.org/resource/";

    public static final String SPARQL_SELECT_LABEL = "SELECT";

    public static final String SPARQL_WHERE_LABEL = "WHERE";

    public static final String SPARQL_ASK_QUERY_IDENTIFIER = "_star_fake";
    
    /***********************************************************************************************************************\
    PARSER - SIMBOLI NON TERMINALI E POS
	\***********************************************************************************************************************/
	
	public static final String S = "S";
	public static final String SBARQ = "SBARQ";
	public static final String NP = "NP";
	public static final String NN = "NN";
	public static final String NNS = "NNS";
	public static final String NNP = "NNP";
	public static final String NNPS = "NNPS";
	
	/***********************************************************************************************************************\
	             LINGUA
	\***********************************************************************************************************************/
	
	public static final String ENGLISH = "en";

}
