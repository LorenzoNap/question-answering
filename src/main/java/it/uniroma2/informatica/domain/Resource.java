package it.uniroma2.informatica.domain;

/**
 * Created by Giovanni Lorenzo Napoleoni on 13/12/2014.
 */
public class Resource extends TripleElement {

    private String mValue;

    public Resource(String mValue) {
        this.mValue = mValue;
    }

    public String getmValue() {
        return mValue;
    }

    public void setmValue(String mValue) {
        this.mValue = mValue;
    }

	@Override
	public String getRepresentation() {
		return mValue;
	}
}
