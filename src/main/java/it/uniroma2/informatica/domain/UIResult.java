package it.uniroma2.informatica.domain;

public class UIResult {
	
	private String mValue;

    public UIResult(String mValue) {
        this.mValue = mValue;
    }

    public String getmValue() {
        return mValue;
    }

    public void setmValue(String mValue) {
        this.mValue = mValue;
    }
}
