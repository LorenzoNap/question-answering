package it.uniroma2.informatica.domain;

import java.util.ArrayList;

/**
 * Created by danielepasquini on 26/01/15.
 */
public class Terminal extends Node {

    public Terminal(String data, ArrayList<Node> children, Node parent) {
        super(data, children, parent);
    }
}
