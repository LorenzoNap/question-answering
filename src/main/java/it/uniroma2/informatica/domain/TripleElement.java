package it.uniroma2.informatica.domain;

public abstract class TripleElement {
	public abstract String getRepresentation();
}
