package it.uniroma2.informatica.domain;

import it.uniroma2.art.owlart.model.ARTNode;

/**
 * Created by Giovanni Lorenzo Napoleoni on 16/12/2014.
 */
public class QueryResult {

    private ARTNode mValue;

    public QueryResult(ARTNode mValue) {
        this.mValue = mValue;
    }

    public ARTNode getValue() {
        return mValue;
    }

    public void setValue(ARTNode value) {
        this.mValue = value;
    }
}
