package it.uniroma2.informatica.domain;

/**
 * Created by Giovanni Lorenzo Napoleoni on 13/12/2014.
 * Modified by Valerio Martella and Francesco Wood on 15/12/2014
 */
public class Query {

    private Triple mTriple;

    public Query(Triple mTriple) {
        this.mTriple = mTriple;
    }

    public Triple getmTriple() {
        return mTriple;
    }

    public void setmTriple(Triple mTriple) {
        this.mTriple = mTriple;
    }
    
    public String toString(){
    	return mTriple.toString();
    }
}
