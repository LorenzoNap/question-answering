package it.uniroma2.informatica.domain;

import it.uniroma2.art.owlart.model.ARTNode;

/**
 * Created by Lorenzo on 18/01/2015.
 */
public class ConcreteTriple {
    private ARTNode mSubject;
    private ARTNode mPredicate;
    private ARTNode mObject;

    public ConcreteTriple(ARTNode mSubject, ARTNode mPredicate, ARTNode mObject) {
        this.mPredicate = mPredicate;
        this.mObject = mObject;
        this.mSubject = mSubject;
    }

    public ARTNode getmSubject() {
        return mSubject;
    }

    public void setmSubject(ARTNode mSubject) {
        this.mSubject = mSubject;
    }

    public ARTNode getmPredicate() {
        return mPredicate;
    }

    public void setmPredicate(ARTNode mPredicate) {
        this.mPredicate = mPredicate;
    }

    public ARTNode getmObject() {
        return mObject;
    }

    public void setmObject(ARTNode mObject) {
        this.mObject = mObject;
    }
    
    @Override
    public String toString(){
    	return mSubject.getNominalValue() + " " + mPredicate.getNominalValue() + " " + mObject.getNominalValue();
    }
}
