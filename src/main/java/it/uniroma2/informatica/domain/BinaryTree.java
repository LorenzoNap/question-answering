package it.uniroma2.informatica.domain;

/**
 * Created by danielepasquini on 21/01/15.
 */
public class BinaryTree {
    private Node mRoot;

    public BinaryTree(Node root){
        this.mRoot = root;
    }

    public Node getRoot() {
        return mRoot;
    }

    public void setRoot(Node root) {
        this.mRoot = root;
    }
}
