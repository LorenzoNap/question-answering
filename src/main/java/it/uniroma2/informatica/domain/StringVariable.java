package it.uniroma2.informatica.domain;

/**
 * Created by Lorenzo on 18/01/2015.
 */
public class StringVariable extends TripleElement{

    private String mValue;

    public StringVariable(String mValue) {
        this.mValue = mValue;
    }

    public String getmValue() {
        return mValue;
    }

    public void setmValue(String mValue) {
        this.mValue = mValue;
    }

    @Override
    public String getRepresentation() {
        return mValue;
    }
}
