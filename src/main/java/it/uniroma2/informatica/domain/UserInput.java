package it.uniroma2.informatica.domain;

/**
 * Created by Giovanni Lorenzo Napoleoni on 13/12/2014.
 */
public class UserInput {

    private String mInput;

    public UserInput(String mInput) {
        this.mInput = mInput;
    }

    public String getmInput() {
        return mInput;
    }

    public void setmInput(String mInput) {
        this.mInput = mInput;
    }
}
