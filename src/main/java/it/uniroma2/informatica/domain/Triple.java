package it.uniroma2.informatica.domain;

/**
 * Created by Giovanni Lorenzo Napoleoni on 13/12/2014.
 */
public class Triple {

    private TripleElement mSubject;
    private TripleElement mPredicate;
    private TripleElement  mObject;

    public Triple(TripleElement mSubject,TripleElement mPredicate, TripleElement mObject) {
        this.mSubject = mSubject;
        this.mPredicate = mPredicate;
        this.mObject = mObject;
    }
    
    public TripleElement getmSubject() {
        return mSubject;
    }

    public void setmSubject(TripleElement mSubject) {
        this.mSubject = mSubject;
    }

    public TripleElement getmPredicate() {
        return mPredicate;
    }

    public void setmPredicate(TripleElement mPredicate) {
        this.mPredicate = mPredicate;
    }

    public TripleElement getmObject() {
        return mObject;
    }

    public void setmObject(TripleElement mObject) {
        this.mObject = mObject;
    }

 
    
    public String toString(){
    	
    	String toReturn = new String();
    	
		if(mSubject instanceof Variable){
			toReturn += ((Variable)mSubject).getId() + " ";
		} else {
			toReturn += ((Resource)mSubject).getmValue() + " ";
		}
		
		if(mPredicate instanceof Variable){
			toReturn += ((Variable)mPredicate).getId() + " ";
		} else {
			toReturn += ((Resource)mPredicate).getmValue() + " ";
		}
		
		if(mObject instanceof Variable){
			toReturn += ((Variable)mObject).getId() + " ";
		} else {
			toReturn += ((Resource)mObject).getmValue() + " ";
		}
    	
		return toReturn;
    }
}
