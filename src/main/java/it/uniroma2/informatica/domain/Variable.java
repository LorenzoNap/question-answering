package it.uniroma2.informatica.domain;

public class Variable extends TripleElement {

	private static Integer mBaseId = 1;

    private Integer mId;
	
	public Variable(){
        mId = mBaseId;
        incrementId();
    }
	
	public static void incrementId(){
		mBaseId++;
	}
	
	public static void resetId(){
		mBaseId = 1;
	}
	
	public Integer getId(){
		return mId;
	}

	@Override
	public String getRepresentation() {
		return mId.toString();
	}
	
}

