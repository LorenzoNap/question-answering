package it.uniroma2.informatica.systemController;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.QueryEvaluationException;
import it.uniroma2.art.owlart.exceptions.UnsupportedQueryLanguageException;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.impl.ARTURIResourceEmptyImpl;
import it.uniroma2.art.owlart.query.MalformedQueryException;
import it.uniroma2.informatica.configurationManager.ConfigurationHandler;
import it.uniroma2.informatica.domain.*;
import it.uniroma2.informatica.exceptions.EmptyStringException;
import it.uniroma2.informatica.exceptions.InternalParserException;
import it.uniroma2.informatica.exceptions.NotWellFormattedString;
import it.uniroma2.informatica.exceptions.NullStringException;
import it.uniroma2.informatica.parser.ParserManager;
import it.uniroma2.informatica.parser.SynParser;
import it.uniroma2.informatica.parser.TemplateXmlParser;
import it.uniroma2.informatica.resultExtractor.ResultExtractorManager;
import it.uniroma2.informatica.synonymer.SynonymerManager;
import it.uniroma2.informatica.uimanager.UIManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * Controller principale del sistema. System Controller gestisce il flusso di esecuzione per rispondere alla domanda effettuata dall'utente.
 * Principal flow in executeApplication.
 * @author Ambra Abdullahi & Riccardo Gambella.
 */
public class SystemController {
	static Logger log = Logger.getLogger(SystemController.class.getName());

	private UIManager uiManager;
	private ConfigurationHandler configurationHandler;
	private ParserManager parserManager;
	private ResultExtractorManager resultExtractorManager;
	private SynonymerManager synonimerManager;

	public SystemController(){

		// Initialize uiManager
		uiManager = UIManager.getIstance();

		// Initialize configuration handler
		configurationHandler = getConfigurationHandler();

		// Initialize ResultExtractor 
		resultExtractorManager = new ResultExtractorManager();

		// Initialize SynonymerManager
		synonimerManager = new SynonymerManager();

		// Initialize wordnet.database property 

		System.setProperty("wordnet.database.dir", configurationHandler.getWordnetPath());

	}

	/**
	 * Get the configuration handler.
	 * @return
	 */
	private ConfigurationHandler getConfigurationHandler(){
		// Initialize configuration handler.
		ConfigurationHandler configurationHandler = null;

		try {
			configurationHandler = ConfigurationHandler.getConfigurationHandler("configuration.properties");
		} catch (IOException e) {
			uiManager.showErrorOnGUI("Error, try inserting another question");
		}

		return configurationHandler;
	}

	/**
	 * Read input from the User
	 * @return
	 */
	private UserInput readInput(){

		UserInput userInput = null;

		// Read input from User.
		try {
			userInput = uiManager.readInputFromUser();
		} catch (IOException e) {
			uiManager.showErrorOnGUI("Error, try inserting another question");
		}

		log.info("Input from user: " + userInput.getmInput());

		return userInput;
	}


	/**
	 * Parse the input using a specified parser.
	 * @param userInput
	 * @return 
	 */
	private List<Query> parseInput(UserInput userInput){

		List<Query> listQuery = new ArrayList<Query>();
		
		// Check if both parser are selected, then launch an error
		if ((configurationHandler.getTemplateParser()).compareTo("true") == 0 && configurationHandler.getSyntacticParser().compareTo("true") == 0){
			uiManager.showErrorOnGUI("Error, both parser are chosen. Choose only one between templateParser and SyntacticParser.");
		}

		// Call template Xml parser if defined in configuration file.
		else if ((configurationHandler.getTemplateParser()).compareTo("true") == 0){
			
			// Initialize Parser Manager as TemplateXmlParser
			parserManager = new TemplateXmlParser();
			
			try {
				listQuery = parserManager.parseInput(userInput);
			} catch (NullStringException e) {
				log.debug(e.toString());
				uiManager.showErrorOnGUI("Error, a null string has been produced");
			} catch (EmptyStringException e) {
				log.debug(e.toString());
				uiManager.showErrorOnGUI("Error, an empty string is not allowed");
			} catch (NotWellFormattedString e) {
				log.debug(e.toString());
				uiManager.showErrorOnGUI("Error, a not well formatted string has been produced");
			} catch (IndexOutOfBoundsException e) {
				log.debug(e.toString());
				uiManager.showErrorOnGUI("An error in Parse the query");
			} catch (InternalParserException e) {
				log.debug(e.toString());
				uiManager.showErrorOnGUI("Internal Parser Exception");
			}
		} 
		
		// Call syntactic parser if defined in configuration file.
		else if ((configurationHandler.getSyntacticParser()).compareTo("true") == 0){
				
			// Initialize Parser Manager as TemplateXmlParser
			parserManager = new SynParser();
			
			try {
				listQuery = parserManager.parseInput(userInput);
			} catch (NullStringException e) {
				log.debug(e.toString());
				uiManager.showErrorOnGUI("Error, a null string has been produced");
			} catch (EmptyStringException e) {
				log.debug(e.toString());
				uiManager.showErrorOnGUI("Error, an empty string is not allowed");
			} catch (NotWellFormattedString e) {
				log.debug(e.toString());
				uiManager.showErrorOnGUI("Error, a not well formatted string has been produced");
			} catch (IndexOutOfBoundsException e) {
				log.debug(e.toString());
				uiManager.showErrorOnGUI("An error in Parse the query");
			} catch (InternalParserException e) {
				log.debug(e.toString());
				uiManager.showErrorOnGUI("Internal Parser Exception. UIMA Exception.");
			}
		} else {
			uiManager.showErrorOnGUI("Error, undefined parser. Choose one between TemplateXmlParser"
					+ "and SyntacticParser");
		}

		log.info("Parsed query: " + (listQuery.get(0)).toString());

		return listQuery;
	}


	/**
	 * Execute a list of query and return the corresponding queryResults.
	 * @param listQuery
	 * @return
	 */
	private List<QueryResult> executeQuery(List<Query> listQuery){
		// Get query results from ontology.
		List<QueryResult> queryResults = null;
		try {
			queryResults = resultExtractorManager.executeQuery(listQuery);
		} catch (UnsupportedQueryLanguageException e) {
			log.debug(e.toString());
			uiManager.showErrorOnGUI("An error related to the SPARQL query has occurred");
		} 
		catch (QueryEvaluationException | MalformedQueryException | ModelCreationException | ModelAccessException e) {
			log.debug(e.toString());
			uiManager.showErrorOnGUI("An error related to the SPARQL query has occurred");
		}

		return queryResults;
	}
	
	/**
	 * Find results looking at the triple related to the resource object.
	 * @param resourcePredicate
	 * @param listAllTriples
	 * @param askQuery
	 * @return List results founded, or true for an askQuery
	 */
	private List<QueryResult> getResultsFromAllTriples(String resourcePredicate, List<ConcreteTriple> listAllTriples, boolean askQuery){
		
		List<QueryResult> queryResults = new ArrayList<QueryResult>();
		
		for(ConcreteTriple triple : listAllTriples){
			ARTNode predicate = triple.getmPredicate();
			if(predicate.isURIResource()){
				ARTURIResource predicateAsURI = predicate.asURIResource();
				if(resourcePredicate.equals(predicateAsURI.getLocalName())){
					
					// Print matched triples.
					getUiManager().getGui().addLog("Founded triple: " + triple.toString());
					
					// Manage ASK Query. If there is a matching triple then return true.
					if(askQuery){
						ARTNode nodeASK = new ARTURIResourceEmptyImpl("true");
	                    QueryResult queryResult = new QueryResult(nodeASK);
	                    queryResults.add(queryResult);
	                    return queryResults;
					}
					
					queryResults.add(new QueryResult(triple.getmObject()));
				}
			}
		}
		
		return queryResults;
	}

	/**
	 * Find list of results using Synonymer: looking synonyms, hyponyms at a maximum depth level and hypernyms at a maximum height level.
	 * @param listQuery
	 * @return
	 */
	private List<QueryResult> findResultsUsingSynonymer(List<Query> listQuery) {

		getUiManager().getGui().addLog("Utilizzo synoymer");
		// Initialize queryResults.
		List<QueryResult> queryResults = new ArrayList<QueryResult>();

		//TODO: Support list of query of multiple sizes.
		if(listQuery.size() >= 2 && listQuery.size() == 0){
			return queryResults;
		}
		
		// Get Properties associated to the object resource.
		
		List<ConcreteTriple> listAllTriples = new ArrayList<ConcreteTriple>();
		
		TripleElement object = listQuery.get(0).getmTriple().getmObject();
		Resource resourceObject = new Resource("");
		if(object instanceof Resource){
			resourceObject= (Resource)object;
		}
		
		// Get all triples associated with the specific resource.
		try {
			listAllTriples = resultExtractorManager.getPropertiesOfResource(resourceObject.getmValue());
		} catch (UnsupportedQueryLanguageException | QueryEvaluationException
				| MalformedQueryException | ModelCreationException
				| ModelAccessException e) {
			getUiManager().showErrorOnGUI("An error related to the SPARQL query has occurred");
		}
		
		// Check if there is an ASK query.
		boolean askQuery = resultExtractorManager.getSparqlManager().isAnAskQuery(listQuery);
		
		System.out.println("ASK query: " + askQuery);

		
		// Get resource predicate from first query.
		TripleElement predicate = listQuery.get(0).getmTriple().getmPredicate();
		Resource resourcePredicate = new Resource("");
		if(predicate instanceof Resource){
			resourcePredicate = (Resource)predicate;
		}
		
		// Look for an answer using synonyms
		// Fetch synonyms.
		List<String> synonyms = new ArrayList<String>();
		synonyms = synonimerManager.fetchSynonyms(resourcePredicate);
		getUiManager().getGui().addLog("Lista sinonimi: " + synonyms);

		// TODO: Integrate list of prop da DBPedia.
		for (String synonym : synonyms){ 
			
			// Get all results associated with founded synonym.
			queryResults = getResultsFromAllTriples(synonym, listAllTriples, askQuery);

			// If results are found, return them.
			if(!queryResults.isEmpty()){
				getUiManager().getGui().addLog("Results founded at depth level: " + 0 + ".\n");
				return queryResults;
			}
		}	


		// Look for an answer using hyponyms.
		List<String> hyponyms = new ArrayList<String>();

		Integer maxHyponymDepthLevel = configurationHandler.getMaxHyponymDepthLevel();

		for (Integer hyponymDepthLevel = 1; hyponymDepthLevel <= maxHyponymDepthLevel; hyponymDepthLevel++) {

			hyponyms = synonimerManager.fetchHyponyms(resourcePredicate, hyponymDepthLevel);

			// No other results possible. No hyponyms.
			if(hyponyms.isEmpty()){
				getUiManager().getGui().addLog("No other hyponyms");
				log.info("No other hyponyms");
				break;
			}

			getUiManager().getGui().addLog("Lista iponimi a depth level: " + hyponymDepthLevel 
					+ " " + hyponyms);
			
			for (String hyponym : hyponyms){  
				
				// Get all results associated with founded hyponym.
				queryResults = getResultsFromAllTriples(hyponym, listAllTriples, askQuery);

				// If results are found, return them.
				if(!queryResults.isEmpty()){
					
					getUiManager().getGui().addLog("Risultati trovati a depth level: " + hyponymDepthLevel + ".\n");
					return queryResults;
				}
			}	
		}

		// Look for an answer using hypernyms.
		List<String> hypernyms = new ArrayList<String>();

		Integer maxhypernymHeightLevel = configurationHandler.getMaxHypernymHeightLevel();

		for (Integer hypernymHeightLevel = 1; hypernymHeightLevel <= maxhypernymHeightLevel; hypernymHeightLevel++) {

			hypernyms = synonimerManager.fetchHypernyms(resourcePredicate, hypernymHeightLevel);

			// No other results possible. No hypernyms.
			if(hypernyms.isEmpty()){
				getUiManager().getGui().addLog("No other hypernyms");
				log.info("No other hypernyms");
				break;
			}
			
			getUiManager().getGui().addLog("Lista iperonimi at height level: " + hypernymHeightLevel 
					+ " " + hypernyms);

			for (String hypernym : hypernyms){  
				
				// Get all results associated with founded hypernym.
				queryResults = getResultsFromAllTriples(hypernym, listAllTriples, askQuery);

				// If results are found, return them.
				if(!queryResults.isEmpty()){
					getUiManager().getGui().addLog("Risultati trovati a height level: " + hypernymHeightLevel + ".\n");
					return queryResults;
				}
			}	

		}
		
		// At this point queryResults is empty: no results using synonymer has been found.

		// Manage for ASK query: return false if no results has been found.
		if(askQuery){
			ARTNode nodeASK = new ARTURIResourceEmptyImpl("false");
            QueryResult queryResult = new QueryResult(nodeASK);
            queryResults.add(queryResult);
            return queryResults;
		}

		return queryResults;
	}

	/**
	 * Visualize results to the user.
	 * @param queryResults
	 */
	private void visualizeResults(List<QueryResult> queryResults){
		uiManager.visualizeResults(queryResults);
	}

	private boolean askforTermination(){

		boolean termination = true;

		try {
			termination = uiManager.askforTermination();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return termination;
	}

	/**
	 * Execute all flow in the application.
	 */
	public void executeApplication() {

		do {

			UserInput userInput = readInput();

			// Reset variable static Id.
			Variable.resetId();

			List<Query> listQuery = parseInput(userInput);

			List<QueryResult> queryResults = executeQuery(listQuery);

			// Call synonymer only if there are no results.
			if (((configurationHandler.getSynonimer()).compareTo("true") == 0)
					&& (queryResults.isEmpty())){
				queryResults = findResultsUsingSynonymer(listQuery);
			}

			if ((configurationHandler.getVisualizeResultToUser()).compareTo("true") == 0){
				visualizeResults(queryResults);
			}

		} while (!askforTermination()); // Ask for termination return false if user answer yes.
	}

	/**
	 * Execute all flow in the application.
	 */
	public List<QueryResult> executeApplication(String inputQuestion) {

		getUiManager().getGui().addLog("Execute systemController");

		UserInput userInput = new UserInput(inputQuestion);

		// Reset variable static Id.
		Variable.resetId();

		List<Query> listQuery = parseInput(userInput);

		List<QueryResult> queryResults = executeQuery(listQuery);

		// Call synonymer only if there are no results.
		if (((configurationHandler.getSynonimer()).compareTo("true") == 0)
				&& (queryResults.isEmpty())){
			queryResults = findResultsUsingSynonymer(listQuery);
		}

		if ((configurationHandler.getVisualizeResultToUser()).compareTo("true") == 0){
			visualizeResults(queryResults);

		}
		return queryResults;

		/*if ((configurationHandler.getSaveResultsOnFile()).compareTo("true") == 0){
			saveResultsOnFile(queryResults);
		}*/

	}

	public ResultExtractorManager getResultExtractorManager() {
		return resultExtractorManager;
	}

	public void setResultExtractorManager(ResultExtractorManager resultExtractorManager) {
		this.resultExtractorManager = resultExtractorManager;
	}

	public UIManager getUiManager() {
		return uiManager;
	}

	public void setUiManager(UIManager uiManager) {
		this.uiManager = uiManager;
	}

	public void setConfigurationHandler(ConfigurationHandler configurationHandler) {
		this.configurationHandler = configurationHandler;
	}

	public ConfigurationHandler getInstanceOfConfigurationHandler(){
		return configurationHandler;
	}
}
